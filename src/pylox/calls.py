import abc
import time

from environment import Environment
from stmt import ReturnStatement


# Anything that can be called needs to implement the Callable interface.
class Callable(abc.ABC):
    @abc.abstractmethod
    def arity(self):
        ...

    @abc.abstractmethod
    def call(self, interpreter, arguments):
        ...


class FunctionCall(Callable):
    def __init__(self, declaration, closure, is_initialiser=False):
        self.declaration = declaration
        self.closure = closure
        self.is_initialiser = is_initialiser

    def __repr__(self):
        return f"<fn {self.declaration.name.lexeme}>"

    def arity(self):
        return len(self.declaration.params)

    def bind(self, instance):
        environment = Environment(self.closure)
        environment.define("this", instance)
        return FunctionCall(self.declaration, environment, self.is_initialiser)

    def call(self, interpreter, arguments):
        environment = Environment(self.closure)

        for param, arg in zip(self.declaration.params, arguments):
            environment.define(param.lexeme, arg)

        block = self.declaration.body.statements

        try:
            interpreter.execute_block(block, environment)
        except ReturnStatement as return_statement:
            if self.is_initialiser:
                return self.closure.get_at("this", 0)

            return return_statement.value

        # explicitly return "this" from initialisers
        if self.is_initialiser:
            return self.closure.get_at("this", 0)

        return None


# Implements the native 'clock' function
class Clock(Callable):
    def __repr__(self):
        return "<native fn clock>"

    def arity(self):
        return 0

    def call(self, interpreter, arguments):
        return time.time()
