class VariableAssignError(Exception):
    pass


class VariableGetError(Exception):
    pass


class Environment:
    def __init__(self, enclosing=None):
        self.enclosing = enclosing
        self.values = dict()

    def global_env(self):
        # The top environment is the 'global' environment
        if self.enclosing:
            return self.enclosing.global_env()
        return self

    def assign(self, name, value):
        if name.lexeme in self.values:
            self.values[name.lexeme] = value
            return

        if self.enclosing:
            self.enclosing.assign(name, value)
            return

        msg = f"Assignment to undefined variable '{name.lexeme}'"
        raise VariableAssignError(msg)

    def assign_at(self, name, value, offset):
        env = self
        for _ in range(offset):
            env = env.enclosing
        env.values[name.lexeme] = value

    def define(self, lexeme, value):
        self.values[lexeme] = value

    def get(self, name):
        if name.lexeme in self.values:
            return self.values[name.lexeme]

        if self.enclosing:
            return self.enclosing.get(name)

        msg = f"Retrieving unknown variable '{name.lexeme}'"
        raise VariableGetError(msg)

    def get_at(self, name, offset):
        # Move to offset number of parent environment and directly extract the
        # variable. If the resolver works OK, the variable will be present.
        env = self
        for o in range(offset):
            env = env.enclosing
        return env.values[name]
