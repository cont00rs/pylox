import logger
from expr import (
    Assign,
    Binary,
    Call,
    Get,
    Grouping,
    Literal,
    Logical,
    Set,
    Super,
    This,
    Unary,
    Variable,
)
from stmt import Block, Class, Expression, Function, If, Print, Return, Var, While
from tokens import Tokens


class ParserError(Exception):
    pass


class Parser:
    def __init__(self, tokens):
        self.tokens = tokens
        self.current = 0
        self.had_error = False
        self.max_parameter_count = 255

    def __call__(self):
        statements = []

        while not self.done():
            if statement := self.declaration():
                statements.append(statement)

        return statements

    def recurse(self, tokens, nested_grammer):
        expr = nested_grammer()

        while self.match(*tokens):
            operator = self.previous()
            right = nested_grammer()
            expr = Binary(expr, operator, right)

        return expr

    def declaration(self):
        try:
            if self.match(Tokens.CLASS):
                return self.class_declaration()
            elif self.match(Tokens.FUN):
                return self.fun_declaration("function")
            elif self.match(Tokens.VAR):
                return self.var_declaration()
            else:
                return self.statement()
        except ParserError:
            self.synchronise()
            return None

    def class_declaration(self):
        name = self.consume(Tokens.IDENTIFIER, "Expect class name.")

        superclass = None
        if self.match(Tokens.LESS):
            self.consume(Tokens.IDENTIFIER, "Expect superclass name.")
            superclass = Variable(self.previous())

        self.consume(Tokens.LEFT_BRACE, "Expect '{' before class body.")

        methods = []
        while not self.check(Tokens.RIGHT_BRACE) and not self.done():
            methods.append(self.fun_declaration("method"))

        self.consume(Tokens.RIGHT_BRACE, "Expect '}' after class body.")
        return Class(name, superclass, methods)

    def fun_declaration(self, kind):
        name = self.consume(Tokens.IDENTIFIER, f"Expect {kind} name.")
        self.consume(Tokens.LEFT_PAREN, "Expect '(' after {kind} name.")

        parameters = []
        if not self.check(Tokens.RIGHT_PAREN):
            at_least_once = True
            while at_least_once or self.match(Tokens.COMMA):
                param = self.consume(Tokens.IDENTIFIER, "Expect parameter name.")
                parameters.append(param)
                at_least_once = False
        self.consume(Tokens.RIGHT_PAREN, "Expect ')' after parameters.")

        if len(parameters) >= self.max_parameter_count:
            self.error(
                self.peek(),
                f"Can't have more than {self.max_parameter_count} parameters.",
            )

        self.consume(Tokens.LEFT_BRACE, f"Expect '{{' before {kind} body.")
        body = self.block()

        return Function(name, tuple(parameters), body)

    def var_declaration(self):
        name = self.consume(Tokens.IDENTIFIER, "Expect variable name.")
        initialiser = None

        if self.match(Tokens.EQUAL):
            initialiser = self.expression()

        self.consume(Tokens.SEMICOLON, "Expect ';' after variable declaration.")
        return Var(name, initialiser)

    def statement(self):
        if self.match(Tokens.FOR):
            return self.for_statement()
        elif self.match(Tokens.IF):
            return self.if_statement()
        elif self.match(Tokens.PRINT):
            return self.print_statement()
        elif self.match(Tokens.RETURN):
            return self.return_statement()
        elif self.match(Tokens.WHILE):
            return self.while_statement()
        elif self.match(Tokens.LEFT_BRACE):
            return self.block()
        else:
            return self.expression_statement()

    def block(self):
        statements = []
        while not (self.check(Tokens.RIGHT_BRACE) or self.done()):
            statements.append(self.declaration())
        self.consume(Tokens.RIGHT_BRACE, "Expect '}' after block.")
        return Block(tuple(statements))

    def for_statement(self):
        # Parse the for-loop
        self.consume(Tokens.LEFT_PAREN, "Expect '(' after 'for'.")

        if self.match(Tokens.SEMICOLON):
            initializer = None
        elif self.match(Tokens.VAR):
            initializer = self.var_declaration()
        else:
            initializer = self.expression_statement()

        condition = self.expression() if not self.check(Tokens.SEMICOLON) else None
        self.consume(Tokens.SEMICOLON, "Expect ';' after loop condition.")

        increment = self.expression() if not self.check(Tokens.RIGHT_PAREN) else None
        self.consume(Tokens.RIGHT_PAREN, "Expect ')' after for clauses.")

        body = self.statement()

        # Desugar the for-loop
        if increment is not None:
            body = Block([body, Expression(increment)])

        if condition is None:
            condition = Literal(True)

        body = While(condition, body)

        if initializer is not None:
            body = Block([initializer, body])

        return body

    def if_statement(self):
        self.consume(Tokens.LEFT_PAREN, "Expect '(' after 'if'.")
        condition = self.expression()
        self.consume(Tokens.RIGHT_PAREN, "Expect ')' after if condition.")

        then_branch = self.statement()
        else_branch = self.statement() if self.match(Tokens.ELSE) else None
        return If(condition, then_branch, else_branch)

    def print_statement(self):
        value = self.expression()
        self.consume(Tokens.SEMICOLON, "Expect ';' after value.")
        return Print(value)

    def return_statement(self):
        keyword = self.previous()

        value = None
        if not self.check(Tokens.SEMICOLON):
            value = self.expression()

        self.consume(Tokens.SEMICOLON, "Expect ';' after return value.")

        return Return(keyword, value)

    def while_statement(self):
        self.consume(Tokens.LEFT_PAREN, "Expect '(' after 'while'.")
        condition = self.expression()
        self.consume(Tokens.RIGHT_PAREN, "Expect ')' after condition.")
        body = self.statement()
        return While(condition, body)

    def expression_statement(self):
        expr = self.expression()
        self.consume(Tokens.SEMICOLON, "Expect ';' after expression.")
        return Expression(expr)

    def expression(self):
        return self.assignment()

    def assignment(self):
        expr = self.logical_or()

        if self.match(Tokens.EQUAL):
            equals = self.previous()
            value = self.assignment()

            if isinstance(expr, Variable):
                return Assign(expr.name, value)
            elif isinstance(expr, Get):
                return Set(expr.obj, expr.name, value)

            self.error(equals, "Invalid assignment target.")

        return expr

    def logical_or(self):
        expr = self.logical_and()

        while self.match(Tokens.OR):
            operator = self.previous()
            right = self.logical_and()
            expr = Logical(expr, operator, right)

        return expr

    def logical_and(self):
        expr = self.equality()

        while self.match(Tokens.AND):
            operator = self.previous()
            right = self.equality()
            expr = Logical(expr, operator, right)

        return expr

    def equality(self):
        tokens = [Tokens.BANG_EQUAL, Tokens.EQUAL_EQUAL]
        return self.recurse(tokens, self.comparison)

    def comparison(self):
        tokens = [Tokens.GREATER, Tokens.GREATER_EQUAL, Tokens.LESS, Tokens.LESS_EQUAL]
        return self.recurse(tokens, self.term)

    def term(self):
        tokens = [Tokens.MINUS, Tokens.PLUS]
        return self.recurse(tokens, self.factor)

    def factor(self):
        tokens = [Tokens.SLASH, Tokens.STAR]
        return self.recurse(tokens, self.unary)

    def unary(self):
        if self.match(Tokens.BANG, Tokens.MINUS):
            operator = self.previous()
            right = self.unary()
            return Unary(operator, right)

        return self.call()

    def call(self):
        expr = self.primary()

        while True:
            if self.match(Tokens.LEFT_PAREN):
                expr = self.finish_call(expr)
            elif self.match(Tokens.DOT):
                name = self.consume(
                    Tokens.IDENTIFIER, "Expect property name after '.'."
                )
                expr = Get(expr, name)
            else:
                break

        return expr

    def finish_call(self, callee):
        arguments = []

        if not self.check(Tokens.RIGHT_PAREN):
            more_arguments = True
            while more_arguments:
                # This is just a choice for pylox, mostly to keep in sync with
                # part 3. Note, this does not require to sync, there is no
                # faulty state of the parser, it merely reports the error.
                if len(arguments) >= self.max_parameter_count:
                    self.error(
                        self.peek(),
                        f"Can't have more than {self.max_parameter_count} arguments.",
                    )

                arguments.append(self.expression())
                more_arguments = self.match(Tokens.COMMA)

        paren = self.consume(Tokens.RIGHT_PAREN, "Expect ')' after arguments")
        return Call(callee, paren, tuple(arguments))

    def primary(self):
        if self.match(Tokens.FALSE):
            return Literal(False)

        if self.match(Tokens.TRUE):
            return Literal(True)

        if self.match(Tokens.NIL):
            return Literal(None)

        if self.match(Tokens.NUMBER, Tokens.STRING):
            return Literal(self.previous().literal)

        if self.match(Tokens.SUPER):
            keyword = self.previous()
            self.consume(Tokens.DOT, "Expect '.' after 'super'.")
            method = self.consume(Tokens.IDENTIFIER, "Expect a superclass method name.")
            return Super(keyword, method)

        if self.match(Tokens.THIS):
            return This(self.previous())

        if self.match(Tokens.IDENTIFIER):
            return Variable(self.previous())

        if self.match(Tokens.LEFT_PAREN):
            expr = self.expression()
            self.consume(Tokens.RIGHT_PAREN, "Expect ')' after expression.")
            return Grouping(expr)

        self.error(self.peek(), "Expected another expression.")

    def consume(self, token_kind, message):
        if self.check(token_kind):
            return self.advance()

        self.error(self.peek(), message)

    def match(self, *token_kinds):
        for kind in token_kinds:
            if self.check(kind):
                self.advance()
                return True
        return False

    def check(self, token_kind):
        if self.done():
            return False

        return self.peek().kind == token_kind

    def advance(self):
        if not self.done():
            self.current += 1

        return self.previous()

    def done(self):
        return self.peek().kind == Tokens.EOF

    def peek(self):
        return self.tokens[self.current]

    def previous(self):
        return self.tokens[self.current - 1]

    def error(self, token, message):
        self.had_error = True

        if token.kind == Tokens.EOF:
            msg = f"at line {token.line}: unexpected EOF. {message}"
        else:
            msg = f"at line {token.line}, {token.lexeme}. {message}"

        logger.error_message(msg)
        raise ParserError

    def synchronise(self):
        # move past broken token
        self.advance()

        sync_tokens = [
            Tokens.CLASS,
            Tokens.FOR,
            Tokens.FUN,
            Tokens.IF,
            Tokens.PRINT,
            Tokens.RETURN,
            Tokens.VAR,
            Tokens.WHILE,
        ]

        # aim to find a sync point before EOF
        while not self.done():
            if self.previous().kind == Tokens.SEMICOLON:
                return

            if self.peek().kind in sync_tokens:
                return

            self.advance()
