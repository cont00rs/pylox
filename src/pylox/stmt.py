import abc
from dataclasses import dataclass

from expr import Expr, Variable
from tokens import Token


class Stmt(abc.ABC):
    def accept(self, visitor):
        fn = getattr(visitor, self.__class__.__name__)
        return fn(self)


@dataclass(frozen=True)
class Block(Stmt):
    statements: [Stmt]


@dataclass(frozen=True)
class Expression(Stmt):
    expression: Expr


@dataclass(frozen=True)
class Function(Stmt):
    name: Token
    params: [Token]
    body: [Stmt]


@dataclass(frozen=True)
class Class(Stmt):
    name: Token
    superclass: Variable
    methods: [Function]


@dataclass(frozen=True)
class If(Stmt):
    condition: Expr
    then_branch: Stmt
    else_branch: Stmt


@dataclass(frozen=True)
class Print(Stmt):
    expression: Expr


@dataclass(frozen=True)
class Return(Stmt):
    keyword: Token
    value: Expr


class ReturnStatement(Exception):
    def __init__(self, value):
        self.value = value


@dataclass(frozen=True)
class Var(Stmt):
    name: Token
    initialiser: Expr


@dataclass(frozen=True)
class While(Stmt):
    condition: Expr
    body: Stmt


class Visitor(metaclass=abc.ABCMeta):
    # The required abstractmethods are dynamically attached based on the
    # available subclasses of Stmt. For each subclass a method needs to be
    # provided to satisfy the complete interface.
    ...


for cls in Stmt.__subclasses__():
    setattr(Visitor, cls.__name__, abc.abstractmethod(lambda self, expr: None))

# Required to ensure attached abstractmethods are enforced during
# initialisation in classes inheriting Visitor.
abc.update_abstractmethods(Visitor)
