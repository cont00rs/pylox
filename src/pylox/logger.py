import logging

logging.basicConfig(format="%(levelname)s: %(message)s", level="INFO")


def error_message(message):
    logging.error(message)


def info_message(message):
    logging.info(message)
