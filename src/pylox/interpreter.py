import traceback

import expr
import logger
import stmt
from calls import Callable, Clock, FunctionCall
from environment import Environment, VariableAssignError
from errors import RunTimeError
from instance import Instance
from loxclass import LoxClass
from stmt import ReturnStatement
from tokens import Tokens


class Interpreter(expr.Visitor, stmt.Visitor):
    def __init__(self):
        self.had_runtime_error = False
        self.environment = Environment()
        self.environment.define("clock", Clock())
        self.locals = dict()

    def __call__(self, statements):
        try:
            for statement in statements:
                self.execute(statement)
        except (TypeError, ZeroDivisionError) as e:
            logger.error_message(e)
            self.had_runtime_error = True
            print(traceback.format_exc())
        except (RunTimeError, VariableAssignError) as e:
            logger.error_message(e)
            self.had_runtime_error = True
            print(traceback.format_exc())
            raise e

    def execute(self, stmt):
        stmt.accept(self)

    def resolve(self, expr, depth):
        assert expr not in self.locals
        self.locals[expr] = depth

    def execute_block(self, statements, environment):
        previous = self.environment

        # Switch to block environment for execution of the block statements.
        # Afterwards, restore the environment enclosing this block. Alternative
        # implementation would be to pass the environment to all calls and not
        # switch it out here.
        try:
            self.environment = environment
            for statement in statements:
                self.execute(statement)
        finally:
            self.environment = previous

    def evaluate(self, expr):
        return expr.accept(self)

    # stmt.Visitor requirements
    def Block(self, stmt):
        block_environment = Environment(self.environment)
        self.execute_block(stmt.statements, block_environment)

    def Class(self, stmt):
        # The class is directly made available in the environment: allow for
        # self referencing class definitions.

        superclass = None
        if stmt.superclass:
            superclass = self.evaluate(stmt.superclass)

            if not isinstance(superclass, LoxClass):
                msg = "Superclass must be a class."
                raise RunTimeError(msg)

        self.environment.define(stmt.name.lexeme, None)

        # insert environment containing "super"
        if stmt.superclass:
            self.environment = Environment(self.environment)
            self.environment.define("super", superclass)

        methods = dict()
        for method in stmt.methods:
            is_initialiser = method.name.lexeme == "init"
            fn_call = FunctionCall(method, self.environment, is_initialiser)
            methods[method.name.lexeme] = fn_call

        cls = LoxClass(stmt.name.lexeme, superclass, methods)

        # pop environment for "super"
        if stmt.superclass:
            self.environment = self.environment.enclosing

        self.environment.assign(stmt.name, cls)

    def Expression(self, stmt):
        self.evaluate(stmt.expression)

    def Function(self, stmt):
        # Pass in the current environment too: expose the scope where the
        # function is defined to support closures.
        function = FunctionCall(stmt, self.environment, is_initialiser=False)
        self.environment.define(stmt.name.lexeme, function)
        return None

    def If(self, stmt):
        if self.is_true(self.evaluate(stmt.condition)):
            return self.execute(stmt.then_branch)

        if stmt.else_branch:
            return self.execute(stmt.else_branch)

    def Print(self, stmt):
        value = self.evaluate(stmt.expression)
        logger.info_message(value)

    def Return(self, stmt):
        value = self.evaluate(stmt.value) if stmt.value is not None else None
        raise ReturnStatement(value)

    def Var(self, stmt):
        value = self.evaluate(stmt.initialiser) if stmt.initialiser else None
        self.environment.define(stmt.name.lexeme, value)

    def While(self, stmt):
        while self.is_true(self.evaluate(stmt.condition)):
            self.execute(stmt.body)

    # expr.Visitor requirements
    def Assign(self, expr):
        value = self.evaluate(expr.value)

        if expr in self.locals:
            offset = self.locals[expr]
            self.environment.assign_at(expr.name, value, offset)
        else:
            self.environment.global_env().assign(expr.name, value)

        return value

    def Binary(self, expr):
        left = self.evaluate(expr.left)
        right = self.evaluate(expr.right)

        op = expr.operator.kind

        if op == Tokens.MINUS:
            self.assert_numbers(expr, right, right)
            return left - right
        elif op == Tokens.SLASH:
            self.assert_numbers(expr, right, right)
            try:
                return left / right
            except ZeroDivisionError as e:
                line = expr.operator.line
                msg = f"at line {line}: {e}"
                raise ZeroDivisionError(msg)
        elif op == Tokens.STAR:
            self.assert_numbers(expr, right, right)
            return left * right
        elif op == Tokens.PLUS:
            if isinstance(left, float) and isinstance(right, float):
                return left + right

            if isinstance(left, str) and isinstance(right, str):
                return left + right

            line = expr.operator.line
            name = expr.__class__.__name__
            token = expr.operator.kind.name
            msg = (
                f"at line {line}: {name} operator '{token}' "
                "expects two numbers or strings."
            )
            raise TypeError(msg)
        elif op == Tokens.GREATER:
            self.assert_numbers(expr, right, right)
            return left > right
        elif op == Tokens.GREATER_EQUAL:
            self.assert_numbers(expr, right, right)
            return left >= right
        elif op == Tokens.LESS:
            self.assert_numbers(expr, right, right)
            return left < right
        elif op == Tokens.LESS_EQUAL:
            self.assert_numbers(expr, right, right)
            return left <= right
        elif op == Tokens.BANG_EQUAL:
            return not self.is_equal(left, right)
        elif op == Tokens.EQUAL_EQUAL:
            return self.is_equal(left, right)
        else:
            assert False, "Unreachable Binary interpretation."

    def Call(self, expr):
        callee = self.evaluate(expr.callee)
        arguments = [self.evaluate(argument) for argument in expr.arguments]

        # Anything that is callable, should be a child of Callable.
        if not isinstance(callee, Callable):
            msg = "Can only call functions and classes."
            raise RunTimeError(msg)

        # Make sure arity of callable matches number of found arguments.
        arity = callee.arity()
        if len(arguments) != arity:
            msg = f"Expected {arity} arguments, found {len(arguments)}."
            raise RunTimeError(msg)

        return callee.call(self, arguments)

    def Get(self, expr):
        obj = self.evaluate(expr.obj)

        if not isinstance(obj, Instance):
            msg = "Only instances have properties."
            raise RunTimeError(msg)

        return obj.get(expr.name)

    def Grouping(self, expr):
        return self.evaluate(expr.expression)

    def Literal(self, expr):
        return expr.value

    def Logical(self, expr):
        left = self.evaluate(expr.left)

        if expr.operator.kind == Tokens.OR:
            if self.is_true(left):
                return left
        else:
            if not self.is_true(left):
                return left

        return self.evaluate(expr.right)

    def Set(self, expr):
        obj = self.evaluate(expr.obj)

        if not isinstance(obj, Instance):
            raise RunTimeError(expr.name, "Only instances have fields.")

        value = self.evaluate(expr.value)
        obj.set(expr.name, value)
        return value

    def Super(self, expr):
        offset = self.locals[expr]
        superclass = self.environment.get_at("super", offset)

        # this is in the nested environment, offset by one
        obj = self.environment.get_at("this", offset - 1)

        method = superclass.find_method(expr.method.lexeme)

        if not method:
            msg = f"Undefined property {expr.method.lexeme}."
            raise RunTimeError(msg)

        return method.bind(obj)

    def This(self, expr):
        return self.look_up_variable(expr.keyword, expr)

    def Unary(self, expr):
        right = self.evaluate(expr.right)

        if expr.operator.kind == Tokens.MINUS:
            self.assert_numbers(expr, right)
            return -1 * right
        elif expr.operator.kind == Tokens.BANG:
            return not self.is_true(right)
        else:
            assert False, "Unreachable Unary interpretation."

    def Variable(self, expr):
        return self.look_up_variable(expr.name, expr)

    def look_up_variable(self, name, expr):
        if expr in self.locals:
            offset = self.locals[expr]
            return self.environment.get_at(name.lexeme, offset)

        return self.environment.global_env().get(name)

    def is_true(self, value):
        if value is None:
            return False

        if isinstance(value, bool):
            return value

        return True

    def is_equal(self, a, b):
        if a is None and b is None:
            return True

        if a is None:
            return False

        return a == b

    def assert_numbers(self, expr, *values):
        if all(isinstance(value, float) for value in values):
            return

        line = expr.operator.line
        name = expr.__class__.__name__
        token = expr.operator.kind.name
        types = [type(value) for value in values]
        types = types[0] if len(types) == 1 else types
        msg = (
            f"at line {line}: {name} operator '{token}' expects numbers, got: {types}."
        )
        raise TypeError(msg)
