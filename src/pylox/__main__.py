import argparse
import sys
from pathlib import Path

import lox


def main(args):
    if args.filename:
        path = Path(args.filename)
        return lox.run(path, print_tokens=args.tokens, print_ast=args.ast)

    return lox.prompt(print_tokens=args.tokens, print_ast=args.ast)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="pylox", description="pylox --- crafting interpreters"
    )
    parser.add_argument("filename", nargs="?", help="pylox source file")
    parser.add_argument("--tokens", action="store_true", help="print tokens")
    parser.add_argument("--ast", action="store_true", help="print AST")

    args = parser.parse_args()
    sys.exit(main(args))
