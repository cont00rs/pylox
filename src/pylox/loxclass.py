from calls import Callable
from instance import Instance


class LoxClass(Callable):
    def __init__(self, name, superclass, methods):
        self.name = name
        self.superclass = superclass
        self.methods = methods

    def __repr__(self):
        return self.name

    def arity(self):
        if initialiser := self.find_method("init"):
            return initialiser.arity()

        return 0

    def call(self, interpreter, arguments):
        instance = Instance(self)

        # Invoke the intialiser method 'init' if present
        if initialiser := self.find_method("init"):
            initialiser.bind(instance).call(interpreter, arguments)

        return instance

    def find_method(self, name):
        if name in self.methods:
            return self.methods[name]

        if self.superclass:
            return self.superclass.find_method(name)

        return None
