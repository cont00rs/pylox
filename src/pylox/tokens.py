from enum import IntEnum, auto, unique


class Token:
    def __init__(self, kind, lexeme, line, literal=None):
        self.kind = kind
        self.lexeme = lexeme
        self.line = line
        self.literal = literal

    def __repr__(self):
        token_name = Tokens(self.kind).name
        string = f"Token: {token_name}, lexeme: '{self.lexeme}', line: {self.line}"
        if self.literal:
            string += f" literal: '{self.literal}'"
        return string


@unique
class Tokens(IntEnum):
    # Single-character tokens
    LEFT_PAREN = 0
    RIGHT_PAREN = auto()
    LEFT_BRACE = auto()
    RIGHT_BRACE = auto()
    COMMA = auto()
    DOT = auto()
    MINUS = auto()
    PLUS = auto()
    SEMICOLON = auto()
    SLASH = auto()  # Note: comments start with '//'
    STAR = auto()

    # One or two character tokens
    BANG = auto()
    BANG_EQUAL = auto()
    EQUAL = auto()
    EQUAL_EQUAL = auto()
    GREATER = auto()
    GREATER_EQUAL = auto()
    LESS = auto()
    LESS_EQUAL = auto()

    # Literals
    IDENTIFIER = auto()
    STRING = auto()
    NUMBER = auto()

    # keywords
    AND = auto()
    CLASS = auto()
    ELSE = auto()
    FALSE = auto()
    FUN = auto()
    FOR = auto()
    IF = auto()
    NIL = auto()
    OR = auto()
    PRINT = auto()
    RETURN = auto()
    SUPER = auto()
    THIS = auto()
    TRUE = auto()
    VAR = auto()
    WHILE = auto()

    # end of file
    EOF = auto()


reserved_tokens = {
    "and": Tokens.AND,
    "class": Tokens.CLASS,
    "else": Tokens.ELSE,
    "false": Tokens.FALSE,
    "for": Tokens.FOR,
    "fun": Tokens.FUN,
    "if": Tokens.IF,
    "nil": Tokens.NIL,
    "or": Tokens.OR,
    "print": Tokens.PRINT,
    "return": Tokens.RETURN,
    "super": Tokens.SUPER,
    "this": Tokens.THIS,
    "true": Tokens.TRUE,
    "var": Tokens.VAR,
    "while": Tokens.WHILE,
}
