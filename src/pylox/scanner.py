from logger import error_message
from tokens import Token, Tokens, reserved_tokens


class Scanner:
    def __init__(self, source):
        self.source = source
        self.start = 0
        self.current = 0
        self.line = 0

    def __call__(self):
        while not self.done():
            self.start = self.current
            token = self.scan_token()
            if token is not None:
                yield token

        yield Token(Tokens.EOF, "", self.line, None)

    def advance(self):
        c = self.source[self.current]
        self.current += 1
        return c

    def done(self):
        return self.current >= len(self.source)

    def scan_token(self):
        c = self.advance()

        if c == "(":
            return self.new_token(Tokens.LEFT_PAREN)
        elif c == ")":
            return self.new_token(Tokens.RIGHT_PAREN)
        elif c == "{":
            return self.new_token(Tokens.LEFT_BRACE)
        elif c == "}":
            return self.new_token(Tokens.RIGHT_BRACE)
        elif c == ",":
            return self.new_token(Tokens.COMMA)
        elif c == ".":
            return self.new_token(Tokens.DOT)
        elif c == "-":
            return self.new_token(Tokens.MINUS)
        elif c == "+":
            return self.new_token(Tokens.PLUS)
        elif c == ";":
            return self.new_token(Tokens.SEMICOLON)
        elif c == "*":
            return self.new_token(Tokens.STAR)
        elif c == "!":
            token = Tokens.BANG_EQUAL if self.match("=") else Tokens.BANG
            return self.new_token(token)
        elif c == "=":
            token = Tokens.EQUAL_EQUAL if self.match("=") else Tokens.EQUAL
            return self.new_token(token)
        elif c == "<":
            token = Tokens.LESS_EQUAL if self.match("=") else Tokens.LESS
            return self.new_token(token)
        elif c == ">":
            token = Tokens.GREATER_EQUAL if self.match("=") else Tokens.GREATER
            return self.new_token(token)
        elif c == "/":
            if self.match("/"):
                while self.peek() != "\n":
                    if self.done():
                        break
                    self.advance()
            else:
                return self.new_token(Tokens.SLASH)
        elif c == " " or c == "\r" or c == "\t":
            pass
        elif c == "\n":
            self.line += 1
        elif c == '"':
            return self.scan_string_token()
        elif c.isdigit():
            return self.scan_number_token()
        elif c.isalpha() or c == "_":
            return self.scan_identifier_token()
        else:
            error_message(f"Unsupported pylox character: '{c}'.")

    def scan_string_token(self):
        while self.peek() != '"':
            if self.done():
                break

            if self.peek() == "\n":
                self.line += 1

            self.advance()

        if self.done():
            error_message(f"Unterminated string on line: {self.line}.")
            return

        # move past the closing quote
        self.advance()

        literal = self.source[self.start + 1 : self.current - 1]
        return self.new_token(Tokens.STRING, literal)

    def scan_number_token(self):
        while self.peek().isdigit():
            self.advance()

        # look for a fractional part, consume if present
        if self.peek() == "." and self.peek_next().isdigit():
            self.advance()

            while self.peek().isdigit():
                self.advance()

        literal = float(self.source[self.start : self.current])
        return self.new_token(Tokens.NUMBER, literal)

    def scan_identifier_token(self):
        while self.peek().isalnum() or self.peek() == "_":
            self.advance()

        literal = self.source[self.start : self.current]
        token_type = reserved_tokens.get(literal.lower(), Tokens.IDENTIFIER)
        return self.new_token(token_type, literal)

    def new_token(self, token_kind, literal=None):
        token_text = self.source[self.start : self.current]
        return Token(token_kind, token_text, self.line, literal)

    def match(self, expected):
        if self.done():
            return False

        if self.source[self.current] != expected:
            return False

        self.current += 1
        return True

    def peek(self):
        if self.done():
            return ""
        return self.source[self.current]

    def peek_next(self):
        if self.current + 1 >= len(self.source):
            return ""
        return self.source[self.current + 1]
