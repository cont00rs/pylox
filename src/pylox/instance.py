from errors import RunTimeError


class Instance:
    def __init__(self, loxclass):
        self.loxclass = loxclass
        self.fields = dict()

    def __repr__(self):
        return f"{self.loxclass.name} instance"

    def get(self, name):
        if method := self.loxclass.find_method(name.lexeme):
            return method.bind(self)

        if name.lexeme not in self.fields:
            msg = f"Undefined property '{name.lexeme}'."
            raise RunTimeError(msg)

        return self.fields[name.lexeme]

    def set(self, name, value):
        self.fields[name.lexeme] = value
