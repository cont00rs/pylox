import abc
from dataclasses import dataclass

from tokens import Token


class Expr(abc.ABC):
    def accept(self, visitor):
        fn = getattr(visitor, self.__class__.__name__)
        return fn(self)


@dataclass(frozen=True)
class Assign(Expr):
    name: Token
    value: Expr


@dataclass(frozen=True)
class Binary(Expr):
    left: Expr
    operator: Token
    right: Expr


@dataclass(frozen=True)
class Call(Expr):
    callee: Expr
    paren: Token
    arguments: [Expr]


@dataclass(frozen=True)
class Get(Expr):
    obj: Expr
    name: Token


@dataclass(frozen=True)
class Grouping(Expr):
    expression: Expr


@dataclass(frozen=True)
class Literal(Expr):
    value: ...  # a number or string literal


@dataclass(frozen=True)
class Logical(Expr):
    left: Expr
    operator: Token
    right: Expr


@dataclass(frozen=True)
class Set(Expr):
    obj: Expr
    name: Token
    value: Expr


@dataclass(frozen=True)
class Super(Expr):
    keyword: Token
    method: Token


@dataclass(frozen=True)
class This(Expr):
    keyword: Token


@dataclass(frozen=True)
class Unary(Expr):
    operator: Token
    right: Expr


@dataclass(frozen=True)
class Variable(Expr):
    name: Token


class Visitor(metaclass=abc.ABCMeta):
    # The required abstractmethods are dynamically attached based on the
    # available subclasses of Expr. For each subclass a method needs to be
    # provided to satisfy the complete interface.
    ...


for cls in Expr.__subclasses__():
    setattr(Visitor, cls.__name__, abc.abstractmethod(lambda self, expr: None))

# Required to ensure attached abstractmethods are enforced during
# initialisation in classes inheriting Visitor.
abc.update_abstractmethods(Visitor)
