import expr
import stmt
from tokens import Tokens


class ASTPrinter(expr.Visitor, stmt.Visitor):
    def __init__(self):
        self.indentation = 0

    def __call__(self, statements):
        string = ""
        for statement in statements:
            string += f"{statement.accept(self)}\n"
        return string

    def print(self, expr):
        print(self(expr))

    # Statement requirements
    def Block(self, stmt):
        substrings = []
        for line in self(stmt.statements).splitlines():
            substrings.append(f"\t{line}")
        substring = "\n".join(substrings)

        return f"{{\n{substring}\n}}"

    def Class(self, stmt):
        return self.wrap(f"class {stmt.name.lexeme}\n", *stmt.methods, sep="\n ")

    def Expression(self, stmt):
        return self.wrap("assign", stmt.expression)

    def Function(self, stmt):
        return self.wrap(f"function {stmt.name.lexeme}", stmt.body)

    def If(self, stmt):
        return self.wrap("if", stmt.condition, stmt.then_branch, stmt.else_branch)

    def Print(self, stmt):
        return self.wrap("print", stmt.expression)

    def Return(self, stmt):
        return self.wrap("return", stmt.value)

    def Var(self, stmt):
        return self.wrap(f"var {stmt.name.lexeme}", stmt.initialiser)

    def While(self, stmt):
        return self.wrap("while", stmt.condition, stmt.body)

    # Expression requirements
    def Assign(self, expr):
        return self.wrap(expr.name.lexeme, expr.value)

    def Binary(self, expr):
        return self.wrap(expr.operator.lexeme, expr.left, expr.right)

    def Call(self, expr):
        return self.wrap(expr.callee.name.lexeme, *expr.arguments)

    def Get(self, expr):
        return self.wrap(f"get {expr.name.lexeme}", expr.obj)

    def Grouping(self, expr):
        return self.wrap("group", expr.expression)

    def Literal(self, expr):
        return "nil" if expr.value is None else str(expr.value)

    def Logical(self, expr):
        kind = "and" if expr.operator.kind == Tokens.AND else "or"
        return self.wrap(kind, expr.left, expr.right)

    def Set(self, expr):
        return self.wrap(f"set {expr.name.lexeme}", expr.obj, expr.value)

    def Super(self, expr):
        return self.wrap(f"this {expr.keyword}")

    def This(self, expr):
        return self.wrap(f"this {expr.keyword}")

    def Unary(self, expr):
        return self.wrap(expr.operator.lexeme, expr.right)

    def Variable(self, expr):
        return expr.name.lexeme

    def wrap(self, name, *args, sep=" "):
        string = sep.join(expr.accept(self) if expr else "nil" for expr in args)
        return f"({name} {string})"
