import readline  # noqa: F401 the import makes input() support readline keys
from parser import Parser

from ast_printer import ASTPrinter
from environment import VariableAssignError
from interpreter import Interpreter
from resolver import Resolver
from scanner import Scanner


class Lox:
    def __init__(self, print_tokens=False, print_ast=False):
        self.print_tokens = print_tokens
        self.print_ast = print_ast

        # The interpreter persists subsequent calls to Lox. This is to ensure
        # state persists in REPL calls.
        self.interpreter = Interpreter()

    def __call__(self, source):
        # scanning
        scanner = Scanner(source)
        tokens = list(scanner())

        # parsing
        parser = Parser(tokens)
        ast = parser()

        if self.print_tokens:
            for token in tokens:
                print(token)

        if ast is not None and self.print_ast:
            printer = ASTPrinter()
            printer.print(ast)

        if parser.had_error:
            return parser.had_error

        # apply one pass of static analysis
        resolver = Resolver(self.interpreter)
        ast = resolver(ast)

        if resolver.had_error:
            return resolver.had_error

        # interpretation / evaluation
        self.interpreter(ast)

        return self.interpreter.had_runtime_error


def run(path, print_tokens=False, print_ast=False):
    lox = Lox(print_tokens, print_ast)
    with open(path, "r") as file:
        source = file.read()
    return lox(source)


def prompt(print_tokens=False, print_ast=False):
    lox = Lox(print_tokens, print_ast)
    while True:
        try:
            line = input("> ")
            if line == "":
                continue
            lox(line)
        except EOFError:
            break
        except KeyboardInterrupt:
            print()
        except VariableAssignError:
            continue
