from enum import IntEnum, auto, unique

import expr
import logger
import stmt


@unique
class FunctionType(IntEnum):
    NONE = 0
    FUNCTION = auto()
    METHOD = auto()
    INITIALISER = auto()


@unique
class ClassType(IntEnum):
    NONE = 0
    CLASS = auto()
    SUBCLASS = auto()


class Resolver(expr.Visitor, stmt.Visitor):
    def __init__(self, interpreter):
        self.had_error = False
        self.interpreter = interpreter
        self.scopes = []
        self.current_funtype = FunctionType.NONE
        self.current_clstype = ClassType.NONE

    def __call__(self, statements):
        for statement in statements:
            self.resolve(statement)
        return statements

    def resolve(self, statements):
        if isinstance(statements, stmt.Stmt) or isinstance(statements, expr.Expr):
            statements = [statements]

        for statement in statements:
            statement.accept(self)

    def resolve_function(self, function, funtype):
        # store outer funtype and switch to nested funtype
        enclosing_funtype = self.current_funtype
        self.current_funtype = funtype

        # resolve function
        self.begin_scope()
        for param in function.params:
            self.declare(param)
            self.define(param)
        self.resolve(function.body.statements)
        self.end_scope()

        # restore function type
        self.current_funtype = enclosing_funtype

    def resolve_local(self, expr, name):
        for i, scope in enumerate(reversed(self.scopes)):
            if name.lexeme in scope:
                self.interpreter.resolve(expr, i)
                return

    def begin_scope(self):
        self.scopes.append({})

    def end_scope(self):
        self.scopes.pop()

    def declare(self, name):
        if len(self.scopes) == 0:
            return

        if name.lexeme in self.scopes[-1]:
            msg = f"Already a variable named '{name.lexeme}' in this scope."
            logger.error_message(msg)
            self.had_error = True

        # add variable, mark not ready yet
        self.scopes[-1][name.lexeme] = False

    def define(self, name):
        if len(self.scopes) == 0:
            return

        # variable is present, mark as ready
        assert name.lexeme in self.scopes[-1].keys()
        self.scopes[-1][name.lexeme] = True

    # stmt.Visitor requirements
    def Block(self, stmt):
        self.begin_scope()
        self.resolve(stmt.statements)
        self.end_scope()

    def Class(self, stmt):
        enclosing_clstype = self.current_clstype
        self.current_clstype = ClassType.CLASS

        self.declare(stmt.name)
        self.define(stmt.name)

        if stmt.superclass:
            if stmt.name.lexeme == stmt.superclass.name.lexeme:
                msg = f"A class '{stmt.name.lexeme}' cannot inherit from itself."
                logger.error_message(msg)
                self.had_error = True

            self.current_clstype = ClassType.SUBCLASS
            self.resolve(stmt.superclass)

        # Introduce "super" to a new scope surrounding all methods
        if stmt.superclass:
            self.begin_scope()
            self.scopes[-1]["super"] = True

        # Ensure "this" is present in the subsequent methods
        self.begin_scope()
        self.scopes[-1]["this"] = True

        for method in stmt.methods:
            fntype = (
                FunctionType.INITIALISER
                if method.name.lexeme == "init"
                else FunctionType.METHOD
            )
            self.resolve_function(method, fntype)

        self.end_scope()

        # Close scope for "super"
        if stmt.superclass:
            self.end_scope()

        self.current_clstype = enclosing_clstype

    def Expression(self, stmt):
        self.resolve(stmt.expression)

    def Function(self, stmt):
        # Ensure function name is present in scope to allow recursion.
        self.declare(stmt.name)
        self.define(stmt.name)
        self.resolve_function(stmt, FunctionType.FUNCTION)

    def If(self, stmt):
        self.resolve(stmt.condition)
        self.resolve(stmt.then_branch)
        if stmt.else_branch:
            self.resolve(stmt.else_branch)

    def Print(self, stmt):
        self.resolve(stmt.expression)

    def Return(self, stmt):
        if self.current_funtype == FunctionType.NONE:
            msg = "Cannot return from top-level code."
            logger.error_message(msg)
            self.had_error = True
            return

        if not stmt.value:
            return

        if self.current_funtype == FunctionType.INITIALISER:
            msg = "Cannot return a value from an initialiser."
            logger.error_message(msg)
            self.had_error = True
            return

        self.resolve(stmt.value)

    def Var(self, stmt):
        self.declare(stmt.name)
        if stmt.initialiser:
            self.resolve(stmt.initialiser)
        self.define(stmt.name)

    def While(self, stmt):
        self.resolve(stmt.condition)
        self.resolve(stmt.body)

    # expr.Visitor requirements
    def Assign(self, expr):
        self.resolve(expr.value)
        self.resolve_local(expr, expr.name)

    def Binary(self, expr):
        self.resolve(expr.left)
        self.resolve(expr.right)

    def Call(self, expr):
        self.resolve(expr.callee)
        for argument in expr.arguments:
            self.resolve(argument)

    def Get(self, expr):
        self.resolve(expr.obj)

    def Grouping(self, expr):
        self.resolve(expr.expression)

    def Literal(self, expr):
        pass

    def Logical(self, expr):
        self.resolve(expr.left)
        self.resolve(expr.right)

    def Set(self, expr):
        self.resolve(expr.value)
        self.resolve(expr.obj)

    def Super(self, expr):
        if self.current_clstype == ClassType.NONE:
            msg = "Cannot use 'super' outside of a class."
            logger.error_message(msg)
            self.had_error = True
        elif self.current_clstype != ClassType.SUBCLASS:
            msg = "Cannot use 'super' in a class with no superclass."
            logger.error_message(msg)
            self.had_error = True

        self.resolve_local(expr, expr.keyword)

    def This(self, expr):
        if self.current_clstype == ClassType.NONE:
            msg = "Cannot use 'this' outside of a class."
            logger.error_message(msg)
            self.had_error = True
            return

        self.resolve_local(expr, expr.keyword)

    def Unary(self, expr):
        self.resolve(expr.right)

    def Variable(self, expr):
        if len(self.scopes) > 0 and expr.name.lexeme in self.scopes[-1]:
            is_defined = self.scopes[-1][expr.name.lexeme]
            if not is_defined:
                msg = "Can't read local variable in its own initializer"
                logger.error_message(msg)
                self.had_error = True

        self.resolve_local(expr, expr.name)
