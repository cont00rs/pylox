from parser import Parser

import pytest
from expr import Binary, Call, Grouping, Literal, Logical, Unary
from scanner import Scanner
from stmt import Block, Var
from tokens import Tokens


@pytest.mark.parametrize(
    "source, name, nargs", [("myfunc(a, b, c);", "myfunc", 3), ("f();", "f", 0)]
)
def test_function_call(source, name, nargs):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    expr = ast[0].expression
    assert isinstance(expr, Call)
    assert expr.callee.name.lexeme == name
    assert len(expr.arguments) == nargs


def test_too_many_arguments():
    source = f"f(a{',a'*255});"
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    parser()
    assert parser.had_error


@pytest.mark.parametrize(
    "source, expr",
    [("1;", Literal), ("!2;", Unary), ("1 * 2;", Binary), ("(1 * 2);", Grouping)],
)
def test_single_expression(source, expr):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    assert isinstance(ast[0].expression, expr)


@pytest.mark.parametrize(
    "source, initialiser",
    [("var a;", None), ("var b = 1;", 1)],
)
def test_variable(source, initialiser):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    assert isinstance(ast[0], Var)
    if initialiser:
        assert ast[0].initialiser.value == initialiser
    else:
        assert not ast[0].initialiser


@pytest.mark.parametrize(
    "source, count",
    [
        ("{\n}", 0),
        ("{\nvar a = 1;}", 1),
        ("{\nvar a = 1;\nvar b = 2;\n}", 2),
    ],
)
def test_block(source, count):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    assert isinstance(ast[0], Block)
    assert len(ast[0].statements) == count
    assert all(isinstance(s, Var) for s in ast[0].statements)


def test_logical():
    source = """"hi" or (2 and 3);"""
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    assert isinstance(ast[0].expression, Logical)
    assert ast[0].expression.operator.kind == Tokens.OR
    assert ast[0].expression.right.expression.operator.kind == Tokens.AND


def test_parse_error_expression():
    scanner = Scanner("(1 + ")
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    assert ast == []
