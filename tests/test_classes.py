import logging

import pytest
from lox import Lox
from utils import is_subdict

source_print_name = """
class DevonshireCream {
    serveOn() {
        return "Scones";
    }
}
print DevonshireCream;
"""

source_constructor = """
class Bagel {}
var bagel = Bagel();
print bagel;
"""

source_set_and_get = """
class Bread {}
var b = Bread();
print b;
b.style = "string";
print b.style;
"""

source_egotist = """
class Egotist {
    speak() {
        print this;
    }
}
var method = Egotist().speak;
method();
"""

source_invoke_init = """
var a = 0;
class Foo {
    init() {
        print this;
        a = a + 1;
    }
}

var foo = Foo();
print foo.init();
"""

source_subclass_syntax = """
class Doughnut {}
class BostonCream < Doughnut {}
"""


@pytest.mark.parametrize(
    "source, environment, keys",
    [
        (source_print_name, {}, ["DevonshireCream"]),
        (source_constructor, {}, ["Bagel", "bagel"]),
        (source_set_and_get, {}, ["Bread", "b"]),
        (source_egotist, {}, ["Egotist", "method"]),
        (source_invoke_init, {"a": 2}, []),
        (source_subclass_syntax, {}, ["Doughnut", "BostonCream"]),
    ],
)
def test_lox_source_classes(source, environment, keys):
    lox = Lox()
    error_code = lox(source)
    assert is_subdict(lox.interpreter.environment.values, environment)
    assert all(k in lox.interpreter.environment.values for k in keys)
    assert error_code == 0


source_cake = """
class Cake {
    taste() {
        var adjective = "delicious";
        print "The " + this.flavor + " cake is " + adjective + "!";
    }
}

var cake = Cake();
cake.flavor = "German chocolate";
cake.taste();
"""

source_subclass_methods = """
class Doughnut {
    cook() {
        print "Fry until golden brown.";
    }
}
class BostonCream < Doughnut {}
BostonCream().cook();
"""

source_fn_call_super = """
class Doughnut {
    cook () {
        print "Fry until golden brown.";
    }
}

class BostonCream < Doughnut {
    cook() {
        super.cook();
        print "Pipe full of custard and coat with chocolate.";
    }
}

BostonCream().cook();
"""


@pytest.mark.parametrize(
    "source, message",
    [
        (source_cake, "The German chocolate cake is delicious"),
        (source_invoke_init, "Foo instance"),
        (source_subclass_methods, "Fry until golden brown."),
        (source_fn_call_super, "Fry until golden brown."),
        (source_fn_call_super, "Pipe full of custard and coat with chocolate."),
    ],
)
def test_lox_class_output(caplog, source, message):
    with caplog.at_level(logging.DEBUG):
        lox = Lox()
        error_code = lox(source)
    assert error_code == 0
    assert message in caplog.text
