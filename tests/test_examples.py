from pathlib import Path

import lox


def test_examples_run():
    example_dir = Path("examples")

    for example in example_dir.iterdir():
        assert lox.run(example) == 0
