import pytest
from scanner import Scanner
from tokens import Tokens, reserved_tokens

token_pairs = {
    "(": Tokens.LEFT_PAREN,
    ")": Tokens.RIGHT_PAREN,
    "{": Tokens.LEFT_BRACE,
    "}": Tokens.RIGHT_BRACE,
    ",": Tokens.COMMA,
    ".": Tokens.DOT,
    "-": Tokens.MINUS,
    "+": Tokens.PLUS,
    ";": Tokens.SEMICOLON,
    "*": Tokens.STAR,
    "/": Tokens.SLASH,
    "!": Tokens.BANG,
    "!=": Tokens.BANG_EQUAL,
    "=": Tokens.EQUAL,
    "==": Tokens.EQUAL_EQUAL,
    "<": Tokens.LESS,
    "<=": Tokens.LESS_EQUAL,
    ">": Tokens.GREATER,
    ">=": Tokens.GREATER_EQUAL,
}


@pytest.mark.parametrize("source, token", list(token_pairs.items()))
def test_scan_one_token(source, token):
    scanner = Scanner(source)
    tokens = list(scanner())
    assert len(tokens) == 1 + 1
    assert tokens[0].kind == token
    assert tokens[0].lexeme == source


def test_skip_comment_lines():
    source = """// This is a comment...\n("""
    scanner = Scanner(source)
    tokens = list(scanner())
    assert len(tokens) == 1 + 1
    assert tokens[0].kind == Tokens.LEFT_PAREN


def test_scan_strings():
    source = '"A string\non multiple\nlines"'
    scanner = Scanner(source)
    tokens = list(scanner())
    assert len(tokens) == 1 + 1
    assert tokens[0].kind == Tokens.STRING
    assert tokens[0].literal == source[1:-1]


# note: .1234 and 1234. are both not supported in Lox
@pytest.mark.parametrize("source, value", [("1234", 1234), ("12.34", 12.34)])
def test_scan_number(source, value):
    scanner = Scanner(source)
    token, eof = list(scanner())
    assert token.kind == Tokens.NUMBER
    assert token.literal == value


@pytest.mark.parametrize("source, word", list(reserved_tokens.items()))
def test_reserved_words(source, word):
    scanner = Scanner(source)
    token, eof = list(scanner())
    assert token.kind == word


@pytest.mark.parametrize("source", ["abc", "ab_c", "_abc", "__123"])
def test_identifiers(source):
    scanner = Scanner(source)
    token, eof = list(scanner())
    assert token.kind == Tokens.IDENTIFIER
    assert token.literal == source
