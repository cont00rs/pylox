def is_subdict(big, small):
    return dict(big, **small) == big
