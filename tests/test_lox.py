import pytest
from errors import RunTimeError
from lox import Lox
from utils import is_subdict

source_if_else_block = """
var a = 1;
var b = -1;
if (a >= 1)
  a = 2;
if (b >= 0)
  a = 1;
else
  b = 3;
"""

source_hanging_if_else = """
var a = 1;
if (true)
  if (false)
    print "hello";
  else
    a = 2;
"""

source_conditionals = """
var a = 1 and 1;
var b = 1 and 2;
"""

source_while_statement = """
var a = 0;
var b = 0;
while ( a < 5 ) {
    a = a + 1;
    b = b - 1;
}
"""

source_for_statement = """
var b = 0;
for (var a = 1; a < 10; a = a + 1) {
    b = b + 10 * a;
}
var a = 0;
var c = 0;
for (; c < 10; c = c + 2)
    a = a + 1;
"""

source_fib = """
fun fib(n) {
    if (n <= 1) return n;
    return fib(n - 2) + fib(n - 1);
}

var f = fib(19);
"""

source_closure = """
fun makeCounter() {
    var i = 0;
    fun count() {
        i = i + 1;
        print i;
    }
    return count;
}
var counter = makeCounter();
counter();
counter();
"""

source_closure_scope = """
var a = 1;
{
    fun addA() {
        a = a + 1;
    }
    addA();
    var a = 99;
    addA();
}
"""


@pytest.mark.parametrize(
    "source, environment",
    [
        (source_if_else_block, {"a": 2, "b": 3}),
        (source_hanging_if_else, {"a": 2}),
        (source_conditionals, {"a": 1, "b": 2}),
        (source_while_statement, {"a": 5, "b": -5}),
        (source_for_statement, {"b": 450, "a": 5, "c": 10}),
        (source_fib, {"f": 4181}),
        (source_closure_scope, {"a": 3}),
    ],
)
def test_source_snippet_environment(source, environment):
    # Assert the given environment is a strict subset of the Lox environment
    # after testing the code. This asserts exact values too.
    lox = Lox()
    lox(source)
    assert is_subdict(lox.interpreter.environment.values, environment)


@pytest.mark.parametrize(
    "source, keys",
    [
        (source_closure, ["counter"]),
    ],
)
def test_source_snippet(source, keys):
    # Assert the resulting environment contains certain keys without asserting
    # anything on its value.
    lox = Lox()
    lox(source)
    assert all(k in lox.interpreter.environment.values.keys() for k in keys)


source_subclass_invalid_object = """
var notAClass = "this is not a class";
class Subclass < notAClass {}
"""


@pytest.mark.parametrize("source", [source_subclass_invalid_object])
def test_sorurce_fails(source):
    lox = Lox()
    with pytest.raises(RunTimeError):
        assert not lox(source)


@pytest.mark.parametrize(
    "source", [source_if_else_block, source_hanging_if_else, source_conditionals]
)
def test_ast_printer_does_not_fail(source):
    lox = Lox(print_tokens=True, print_ast=True)
    lox(source)
    assert not lox.interpreter.had_runtime_error
