from parser import Parser

import pytest
from interpreter import Interpreter
from resolver import Resolver
from scanner import Scanner

source_repeated_declaration = """
fun bad() {
   var a = "first";
   var a = "second";
}
"""

source_return_outside_function = """
return "at top level";
"""

source_this_at_top_level = """
print this;
"""

source_this_in_function = """
function notAMethod() {
    print this;
}
"""

source_return_value_in_constructor = """
class Foo {
    init() {
        return "invalid return";
    }
}
"""

source_subclass_same_class = """
class Oops < Oops {}
"""

source_super_in_baseclass = """
class Eclar {
    cook() {
        super.cook();
    }
}
"""

source_super_at_top_level = """
super.notEvenInAClass();
"""


@pytest.mark.parametrize(
    "source",
    [
        source_repeated_declaration,
        source_return_outside_function,
        source_this_at_top_level,
        source_this_in_function,
        source_return_value_in_constructor,
        source_subclass_same_class,
        source_super_in_baseclass,
        source_super_at_top_level,
    ],
)
def test_resolver_errors(source):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    resolver = Resolver(Interpreter())
    resolver(parser())
    assert resolver.had_error
