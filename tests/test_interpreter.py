from parser import Parser

import pytest
from environment import VariableAssignError
from interpreter import Interpreter, RunTimeError
from resolver import Resolver
from scanner import Scanner
from utils import is_subdict

number_ops = ["-", "/", "*", "+", ">", ">=", "<", "<="]


@pytest.mark.parametrize(
    "source", ['-"a";', "!1 + 2;", *[f'1 {op} "a";' for op in number_ops]]
)
def test_interpreter_type_error(source):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    first_expr = ast[0].expression
    interpreter = Interpreter()
    with pytest.raises(TypeError):
        interpreter.evaluate(first_expr)


@pytest.mark.parametrize(
    "source, env",
    [
        ("var a;", {"a": None}),
        ("var a = 1;", {"a": 1}),
        ("var a = 1; a = 2;", {"a": 2}),
        ("var a = 1;\nvar b = 2;", {"a": 1, "b": 2}),
        ("var a = 1;\nvar b = 2;\nvar a = 3;", {"a": 3, "b": 2}),
    ],
)
def test_interpreter_environment(source, env):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    interpreter = Interpreter()
    interpreter(parser())
    assert is_subdict(interpreter.environment.values, env)


def test_interpreter_nested_environment():
    source = """
var a = 1;
{
    var a = 2;
    {
        var b = 3;
        a = a + b;
    }
}
"""
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    ast = parser()
    interpreter = Interpreter()
    resolver = Resolver(interpreter)
    interpreter(resolver(ast))
    # inner a shadows outer a
    assert interpreter.environment.values["a"] == 1
    # inner environments are dropped
    assert "b" not in interpreter.environment.values


@pytest.mark.parametrize(
    "source",
    ["a = 1;", "var a = 1;\nb = 2;", "var a = 1;\n{\nb = 2;\n}\n"],
)
def test_invalid_assignment(source):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    interpreter = Interpreter()
    with pytest.raises(VariableAssignError):
        interpreter(parser())


@pytest.mark.parametrize("source", ['"cannot call this"();'])
def test_invalid_function_call(source):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    interpreter = Interpreter()
    with pytest.raises(RunTimeError):
        interpreter(parser())


def test_native_functions():
    interpreter = Interpreter()
    assert "clock" in interpreter.environment.values


@pytest.mark.parametrize(
    "source, names",
    [
        ("fun add(a, b) {print a + b;}", ["add"]),
        ("fun a() {}; fun b() {};", ["a", "b"]),
    ],
)
def test_function_declaration(source, names):
    scanner = Scanner(source)
    tokens = list(scanner())
    parser = Parser(tokens)
    interpreter = Interpreter()
    interpreter(parser())
    assert all(name in interpreter.environment.values.keys() for name in names)
