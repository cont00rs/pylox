import expr
from ast_printer import ASTPrinter
from tokens import Token, Tokens


def test_ast_print():
    times = Token(Tokens.STAR, "*", 0)
    minus = Token(Tokens.MINUS, "-", 0)

    statements = [
        expr.Binary(
            expr.Unary(minus, expr.Literal(123)),
            times,
            expr.Grouping(expr.Literal(45.67)),
        )
    ]

    printer = ASTPrinter()
    assert printer(statements) == "(* (- 123) (group 45.67))\n"
