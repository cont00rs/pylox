SOURCE=src
TESTS=tests
VENV=venv
PYTHON=$(VENV)/bin/python3

format:
	$(PYTHON) -m isort --profile black $(SOURCE) $(TESTS)
	$(PYTHON) -m black $(SOURCE) $(TESTS) setup.py
	$(PYTHON) -m ruff $(SOURCE) $(TESTS) setup.py

repl:
	$(PYTHON) src/pylox

clean:
	$(RM) -r pylox.egg-info
	$(RM) .coverage
	$(RM) -r .pytest_cache
	$(RM) -r .ruff_cache

distclean: clean
	$(RM) -r $(VENV)

install:
	python3 -m venv $(VENV)
	$(VENV)/bin/pip install -e .

test:
	PYTHONPATH=$(SOURCE)/pylox $(PYTHON) -m pytest $(TESTS)

coverage:
	PYTHONPATH=$(SOURCE)/pylox $(PYTHON) -m coverage run -m pytest $(TESTS)
	$(PYTHON) -m coverage report
