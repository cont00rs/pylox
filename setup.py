from distutils.core import setup

setup(
    name="pylox",
    packages=["src/pylox"],
    package_dir={"pylox": "src/pylox"},
    install_requires=[
        "black",
        "coverage",
        "isort",
        "ruff",
        "pytest",
    ],
)
